<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    public function users()
    {
        return $this->hasMany(User::class);
        //return $this->belongsTo(\App\User::class);
        //belongsTo
        //hasMany
        //relaciones 1 a N

        //hasOne 1 a 1

        //belongsToMany n a m
    }
}
