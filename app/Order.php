<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Order extends Model
{
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function total()
    {
        $total = 0;
        foreach ($this->products as $product){
            $total += $product->price * $product->quantity;
        }
    }
}

?>
