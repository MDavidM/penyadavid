@extends('layouts.app')

@section('content')
    <h1>Lista de roles</h1>
    <a href="/role/create">Nuevo</a>
    <ul>
    @forelse ($roles as $role)
        <li>{{ $role->name }}
            <a href="/roles/{{ $role->id }}/edit">Editar</a>

            <form method="post" action="/roles/{{ $role->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form>
        </li>
    @empty
        <li>No hay roles!!</li>
    @endforelse
    </ul>

    {{ $users->render() }}

@endsection
