@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Grupo de usuarios en sesión</h1>
    <a href="/users/create">Nuevo</a>
    <ul>
    @forelse ($users as $user)
        <li>{{ $user->name }}: {{ $user->email }}
            <a href="/users/{{ $user->id }}/edit">Editar</a>

            <form method="post" action="/users/{{ $user->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                @can ('delete', $user)
                <input type="submit" value="borrar" class="btn btn-primary">
                @endcan
                <a class ="btn btn-success" href="/groups/{{ $user->id }}">Guardar</a>
            </form>
        </li>
    @empty
        <li>No hay usuarios!!</li>
    @endforelse
    </ul>



@endsection
